#!/usr/bin/env python
# coding: utf-8


import pandas as pd

names = "confirmed", "deaths", "recovered"


def load_data(name):
    url = f"https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_{name}_global.csv"
    return pd.read_csv(url)


# Sum to get global data

def get_world(df):
    world = df.sum()[3:].astype("int")
    world.index = pd.DatetimeIndex(world.index)
    return world


world = pd.DataFrame()
for name in names:
    world[name] = get_world(load_data(name))

# world.plot();
world.to_csv("covid19_world.csv", index_label="date", date_format="%Y-%m-%d")


# By country

data_dict = {name: load_data(name) for name in names}

countries = set(data_dict["confirmed"]["Country/Region"])

json_str = "{"

df_countries = pd.DataFrame()

for country in countries:
    df_country = pd.DataFrame(columns=names)
    for name, df_name in data_dict.items():
        ps_country = (
            df_name.loc[df_name["Country/Region"] == country].sum()[4:].astype("int")
        )
        ps_country.index = pd.DatetimeIndex(ps_country.index).strftime("%Y-%m-%d")
        df_country[name] = ps_country
    df_country['date'] = ps_country.index  # Add date in record

    country_json = df_country.to_json(orient='records')
    json_str += f'"{country}":{country_json},'

    country_col = pd.DataFrame(index=df_country['date'])
    country_col['country'] = country
    country_block = country_col.join(df_country, sort=False)
    df_countries = pd.concat([df_countries, country_block])

# Replace last coma by json terminator
json_str = json_str[:-1] + "}"

with open("covid19_countries.json", "w") as f:
    f.write(json_str)

df_countries.to_json('covid19_country.json', orient='records')