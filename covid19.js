var url_fr =
  "https://raw.githubusercontent.com/opencovid19-fr/data/master/dist/chiffres-cles.json";
var url_world =
  "https://boileau.pages.math.unistra.fr/covid19/covid19_world.csv";
var url_country =
  "https://boileau.pages.math.unistra.fr/covid19/covid19_countries.json";

var tomorrow = new Date();
tomorrow.setDate(tomorrow.getDate() + 1);
var month = "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" ")[
  tomorrow.getMonth()
];
var day = tomorrow.getDate();
var year = tomorrow.getFullYear();
var world_layer = ["confirmed", "deaths", "recovered"];
var fr_layer = [
  "casConfirmes",
  "deces",
  "gueris",
  "hospitalises",
  "reanimation",
];
var vis_dict = {
  vis1: {
    title: "Monde",
    source: "JHU",
    type: "world",
    data: { url: url_world },
    ymin: 10,
    ymax: 1000000000,
    start_day: 19,
    start_month: "Jan",
    layer: world_layer,
  },
  vis2: {
    title: "France",
    source: "opencovid19-fr (données hospitalières)",
    type: "fr",
    data: { url: url_fr },
    filter: "",
    ymin: 1,
    ymax: 10000000,
    start_day: 23,
    start_month: "Feb",
    layer: fr_layer,
  },
  vis3: {
    title: "Grand Est",
    source: "opencovid19-fr (données hospitalières)",
    type: "fr",
    data: { url: url_fr },
    filter: "&& datum.deces > 0",
    ymin: 1,
    ymax: 20000,
    start_day: 23,
    start_month: "Feb",
    layer: fr_layer,
  },
  vis4: {
    title: "Bas-Rhin",
    source: "opencovid19-fr (données hospitalières)",
    type: "fr",
    data: { url: url_fr },
    filter: "&& datum.deces > 0",
    ymin: 10,
    ymax: 2000,
    start_day: 5,
    start_month: "Mar",
    layer: fr_layer,
  },
  vis5: {
    title: "Haut-Rhin",
    source: "opencovid19-fr (données hospitalières)",
    type: "fr",
    data: { url: url_fr },
    filter: "&& datum.deces > 0",
    ymin: 10,
    ymax: 2000,
    start_day: 5,
    start_month: "Mar",
    layer: fr_layer,
  },
};

var vis_country = {
  source: "JHU",
  type: "country",
  filter: "datum.confirmed > 0",
  ymin: 10,
  ymax: 100000,
  start_day: 19,
  start_month: "Jan",
  layer: world_layer,
};

function setAllVis() {
  setVisDict();
  setCountryVis();
}

function setVisDict() {
  // reset doc alsace
  document.getElementById("alsace").innerHTML = "";
  // loop on visNames
  var visNames = Object.keys(vis_dict);
  var scale_type = document.querySelector("input[name=scale_type]:checked")
    .value;
  for (const visName of visNames) {
    var vis = vis_dict[visName];
    getVegaVis(vis, visName, scale_type);
  }
}

function resetDiv(divId) {
  var myDiv = document.getElementById(divId);
  if (myDiv) {
    myDiv.remove();
  }
}

function setCountryVis() {
  var request = new XMLHttpRequest();
  request.open("GET", url_country);
  request.responseType = "json";
  request.send();
  request.onload = function () {
    var countries = request.response;
    var countryNames = Object.keys(countries).sort();

    // reset div content
    resetDiv("vis_country");
    var CountryDiv = document.getElementById("country");
    var MenuDiv = document.getElementById("country_menu");
    resetDiv("country_selector_div");
    var selectorDiv = document.createElement("div");
    selectorDiv.setAttribute("id", "country_selector_div");
    var myLabel = document.createElement("label");
    myLabel.setAttribute("for", "country_selector");
    myLabel.setAttribute("id", "country_selector_label");
    myLabel.textContent = "Pays";
    selectorDiv.appendChild(myLabel);
    var mySelect = document.createElement("select");
    mySelect.setAttribute("id", "country_selector");
    for (const countryName of countryNames) {
      var opt = document.createElement("option");
      opt.value = countryName;
      opt.text = countryName;
      if (countryName == 'US') {
        opt.selected = true;
      }
      mySelect.add(opt, null);
    }
    selectorDiv.appendChild(mySelect);
    MenuDiv.appendChild(selectorDiv);
    CountryDiv.appendChild(MenuDiv);

    setVis();

    function setVis() {
      resetDiv("vis_country");
      var countryName = document.getElementById("country_selector").value;
      vis_country["title"] = countryName;
      data_country = countries[countryName];

      // find maximum value
      var ymax = 0;
      for (const record of data_country) {
        if (record["confirmed"] > ymax) {
          ymax = record["confirmed"];
        }
      }
      vis_country["ymax"] = ymax;
      vis_country["data"] = { values: data_country };

      var scale_type = document.querySelector(
        "input[name=scale_type_country]:checked"
      ).value;
      getVegaVis(vis_country, "vis_country", scale_type);
    };
    
    document.getElementById("country_selector").onchange = setVis;
  };
}

// $(function() {
//   $('#country_selector').change(function() {
//       localStorage.setItem('countryName', this.value);
//   });
//   if(localStorage.getItem('countryName')){
//       $('#country_selector').val(localStorage.getItem('countryName'));
//   }
// });

function getVegaVis(vis, visName, scale_type) {
  var VlSpec = {
    $schema: "https://vega.github.io/schema/vega-lite/v4.json",
    description: "Covid19 time series",
    title: {
      text: vis["title"],
      subtitle: "Source : " + vis["source"],
    },
    data: vis["data"],
    width: "container",
    height: "container",
    repeat: {
      layer: vis["layer"],
    },
    spec: {
      mark: {
        type: "point",
        clip: true,
      },
      encoding: {
        x: {
          field: "date",
          title: "Date",
          type: "temporal",
          scale: {
            domain: [
              {
                year: 2020,
                month: vis["start_month"],
                date: vis["start_day"],
              },
              {
                year: year,
                month: month,
                date: day,
              },
            ],
          },
        },
        y: {
          field: { repeat: "layer" },
          title: "Nombre de personnes",
          type: "quantitative",
        },
        tooltip: [
          { field: "date", type: "temporal" },
          { field: { repeat: "layer" }, type: "quantitative" },
        ],
        color: {
          datum: { repeat: "layer" },
          type: "nominal",
        },
      },
    },
  };
  if ("filter" in vis) {
    if (vis["type"] == "fr") {
      filter = "datum.nom == '" + vis["title"] + "' " + vis["filter"];
    } else {
      filter = vis["filter"];
    }
    VlSpec["transform"] = [{ filter: filter }];
  }

  VlSpec["spec"]["encoding"]["y"]["scale"] = { type: scale_type };
  if (scale_type == "log") {
    VlSpec["spec"]["encoding"]["y"]["scale"]["domain"] = [
      vis["ymin"],
      vis["ymax"],
    ];
  }

  if (vis["type"] == "country") {
    parentId = "country";
  } else {
    parentId = "alsace";
  }
  var parent = document.getElementById(parentId);
  var myDiv = document.createElement("div");
  myDiv.setAttribute("id", visName);
  myDiv.setAttribute("class", "vis");
  parent.appendChild(myDiv);
  vegaEmbed("#" + visName, VlSpec);
}

window.onload = function () {
  setVisDict();
  document.getElementById("linear").onchange = setVisDict;
  document.getElementById("log").onchange = setVisDict;
  setCountryVis();
  document.getElementById("linear_country").onchange = setCountryVis;
  document.getElementById("log_country").onchange = setCountryVis;
};
